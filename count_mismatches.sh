#!/bin/bash

# Use this to count the number of mismatches in each Clustalw-aligned .aln file for each CDS.
# First, the number of matches is calculated by counting "*" symbols, excluding white spaces, white lines and line breaks.
# Then, the number of mismatches is calculated by subtracting this number from the CDS length, 
# in turn calculated by subtracting cds_start from cds_stop.
# Run from root of NC_* folders


# Choose which pop you want: "it" or "eur" for within species or "N" for the alignment between the two species.
pop=$1

echo -e "chr\tCDS_start\tCDS_stop\tCDS_length\tmismatches"

for i in $(ls NC*/${pop}*aln); do
	chr=$( echo $i | cut -d":" -f 1 )
	cds_start=$( echo $i | cut -d':' -f 2 | cut -d'-' -f 1 )
	cds_stop=$( echo $i | cut -d'/' -f 1 | cut -d'-' -f 2 )
	cds_length=` expr $cds_stop - $cds_start `
	matches=$( cat $i | grep "*" | tr -d [:space:] | wc -m )
	mismatches=` expr $cds_length - $matches + 1`
	echo -e "$chr\t$cds_start\t$cds_stop\t$cds_length\t$mismatches"
done
