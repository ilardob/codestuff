#!/bin/bash

#################################################################
#                                                               #
#                                                               #                                                                                                     
# 								# 
#								#
#       ____ ____  ____    _          __  __ ____    _      	#
#      / ___|  _ \/ ___|  | |_ ___   |  \/  / ___|  / \     	#
#     | |   | | | \___ \  | __/ _ \  | |\/| \___ \ / _ \    	#
#     | |___| |_| |___) | | || (_) | | |  | |___) / ___ \   	#
#      \____|____/|____/   \__\___/  |_|  |_|____/_/   \_\  	#
#                                                           	#
#       ____ ___  _   _ ____  _____ _   _ ____  _   _ ____  	#	
#      / ___/ _ \| \ | / ___|| ____| \ | / ___|| | | / ___| 	#
#     | |  | | | |  \| \___ \|  _| |  \| \___ \| | | \___ \ 	#
#     | |__| |_| | |\  |___) | |___| |\  |___) | |_| |___) |	#
#      \____\___/|_| \_|____/|_____|_| \_|____/ \___/|____/ 	#
#                                                           	#
#								#
#	Takes a .txt list of coding genomic regions (CDS) in	#
#	the format "chr_name:cds_start:cds_stop\n" and a list 	#
#	of sample names (for now only "flaviae_names.txt" or	#
#	"lucius_names.txt").					#
#								#
#	In the working dir, you must include:			#
#	- GCF_000721915.3_Eluc_V3_genomic.fa			#
#	- pike_nohyb_minDP8_maf0.5_geno0.1_biallelicsnponly	#
#	  .vcf.gz (MUST be bgzipped)				#
#	- the corresponding vcf.gz.tbi index file, if not,	#
#	  generate it with:  tabix -p vcf my.vcf.gz		#
#								#
#	Dependencies:						#
#	- clustalW						#
#	  (http://www.ncbi.nlm.nih.gov/pubmed/17846036)		#
#	- samtools 						#
#	  (http://github.com/samtools/samtools)			#
#	- bcftools						#
#	  (http://github.com/samtools/bcftools)			#
#	- ANDES							#
#	  (https://sourceforge.net/projects/andestools/)	#
#								#
#								#
#	This is a really nasty, hardcoded script so make sure	#
#	to update path names according to where your software	#
#	is installed.						#
#	Also, this script can and should be parallelized.	#
#								#
#	Author: Barbara S. Ilardo				#
#								#
#                                                               #
#################################################################

# Author of ProgressBar : Teddy Skarin
# Input is currentState($1) and totalState($2)
function ProgressBar {
# Process data
	let _progress=(${1}*100/${2}*100)/100
	let _done=(${_progress}*4)/10
	let _left=40-$_done
# Build progressbar string lengths
	_done=$(printf "%${_done}s")
	_left=$(printf "%${_left}s")
printf "\rProgress : [${_done// /\#}${_left// /\-}] ${_progress}%%\n"

}




totalState=$(cat $1|wc -l)
currentState=0


pop=$2
#either you call both pops with 3-letter acronyms and then subset names by the first 12 and not 11 letters, or you create a differential variable for each pop by which to subset the name string.

#if [ $pop == "flaviae_names.txt" ]; then pop_id="ita"; else pop_id="eur"; fi
if [[ $pop == *"laviae"* ]]; then pop_id="it"; else pop_id="eur"; fi
if [[ $pop == *"laviae"* ]]; then counter=11; else counter=12; fi
cat $1|while read cds ; do
	let currentState++
	ProgressBar currentState totalState
	echo -e "making dir ${cds}/\n"
        mkdir $cds
	
	cat $pop|while read ind ; do
		echo $pop_id
		echo -e "\ngenerating fasta for ind ${ind::8} cds $cds"
		samtools faidx GCF_000721915.3_Eluc_V3_genomic.fa $cds | bcftools consensus -I -s $ind pike_nohyb_minDP8_maf0.5_geno0.1_biallelicsnponly.vcf.gz -o ${cds}/${pop_id}_${ind::8}_cds_${cds}.fa
	done
	cd $cds
	echo -e "\ngenerating consensus fasta for pop ${pop_id} cds $cds"
	for i in $(ls ${pop_id}_E*); do header=$(head -n 1 $i); echo -en ">${i::counter}_${header:1}\n" >> ${pop_id}_cds_${cds}.fa; tail -n +2 $i >> ${pop_id}_cds_${cds}.fa; done
	clustalw ${pop_id}_cds_${cds}.fa
	/home/ilardob/bin/ANDES/ClustalALN_to_PositionProfile.pl -a ${pop_id}_cds_${cds}.aln
	/home/ilardob/bin/ANDES/Profile_To_ConsensusFASTA.pl -c ${pop_id}_cds_${cds}_cons.fa -p ${pop_id}_cds_${cds}.prof
	cd ..
	
done
printf '\nFinished!\n'

