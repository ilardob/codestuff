#!/bin/bash

pids=""
for i in range{1..100}
do
	mkdir test_$i
	cd test_$i
	( echo "this is a test file for ${i}" >> test_${i}; sleep 3 ) &  # this creates a subshell and launches its command in background
	pids="$pids $!"
	cd ..
done
wait $pids
echo "all done"
